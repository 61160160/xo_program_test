/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import XO.OOP.Player;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import XO.OOP.Table;
import XO.OOP.Game;

/**
 *
 * @author informatics
 */
public class tableUnitTest {

    public tableUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testRowWin() {
        Player O = new Player();
        Player X = new Player();
        Table table = new Table(X, O);
        table.inputRowCol(1, 1);
        table.inputRowCol(1, 2);
        table.inputRowCol(1, 3);
        assertEquals(true, table.checkWin(1, 1));
    }

    public void testRowWin2() {
        Player O = new Player();
        Player X = new Player();
        Table table = new Table(X, O);
        table.inputRowCol(2, 1);
        table.inputRowCol(2, 2);
        table.inputRowCol(2, 3);
        assertEquals(true, table.checkWin(2, 2));
    }
    public void testRowWin3(){
        Player O = new Player();
        Player X = new Player();
        Table table = new Table(X, O);
        table.inputRowCol(3, 1);
        table.inputRowCol(3, 2);
        table.inputRowCol(3, 3);
        assertEquals(true, table.checkWin(3, 3));
    }

    public void testgetContinue() {
        Game game = new Game();
        assertEquals('Y', game.getContinue());

    }

    public void testSwitchPlayer() {
        Player O = new Player();
        Player X = new Player();
        Table table = new Table(X, O);
        table.switchTurn();
        assertEquals('O', table.getCurrentPlayer().getName());
    }

    public void testColWin() {
        Player O = new Player();
        Player X = new Player();
        Table table = new Table(X, O);
        table.inputRowCol(1, 1);
        table.inputRowCol(2, 1);
        table.inputRowCol(3, 1);
        assertEquals(true, table.checkWin(1, 1));
    }
    public void testColWin2(){
        Player O = new Player();
        Player X = new Player();
        Table table = new Table(X, O);
        table.inputRowCol(1, 2);
        table.inputRowCol(2, 2);
        table.inputRowCol(3, 2);
        assertEquals(true, table.checkWin(2, 2));
    }
    public void testColWin3(){
        Player O = new Player();
        Player X = new Player();
        Table table = new Table(X, O);
        table.inputRowCol(1, 3);
        table.inputRowCol(2, 3);
        table.inputRowCol(3, 3);
        assertEquals(true, table.checkWin(2, 3));
    }

    public void testcheckDiagonal1() {
        Player O = new Player();
        Player X = new Player();
        Table table = new Table(X, O);
        table.inputRowCol(1, 1);
        table.inputRowCol(2, 2);
        table.inputRowCol(3, 3);
        assertEquals(true, table.checkWin(1, 1));
    }

    public void testcheckDiagonal2() {
        Player O = new Player();
        Player X = new Player();
        Table table = new Table(X, O);
        table.inputRowCol(1, 3);
        table.inputRowCol(2, 2);
        table.inputRowCol(3, 1);
        assertEquals(true, table.checkWin(1, 1));
    }

//    public 
}
