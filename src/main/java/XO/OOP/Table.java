package XO.OOP;


public class Table {

    private Player currentPlayer;
    private char[][] data = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
    private Player X;
    private Player O;
    private Player win;
    private Player player;
    private int countTurn = 0;

    public Table(Player X, Player O) {
        O.setName('O');
        X.setName('X');
        this.X = X;
        this.O = O;
        currentPlayer = X;
    }

    public void inputRowCol(int row, int col) {
        data[row - 1][col - 1] = currentPlayer.getName();
        countTurn++;
    }
    
    public Player getWinner() {
        return win;
    }
    
    public boolean checkPos(int row, int col) {
        if ((row > 3 || col > 3) || (row < 1 || col < 1)) {
            System.out.println("re enter the collect row and col");
            return true;
        }
        else if (data[row - 1] [col - 1] != '-' ) {
            System.out.println("This position isn't empty!! Please try again.");
            return true;
        }else{
            
        }
        return false;
    }

    public boolean checkWin(int row, int col) {
        if (checkRow(row)) {
            win = currentPlayer;
            switchTurn();
            return true;
        } else if (checkCol(col)) {
            win = currentPlayer;
            switchTurn();
            return true;
        } else if (checkDiagonal1()) {
            win = currentPlayer;
            switchTurn();
            return true;
        } else if (checkDiagonal2()) {
            win = currentPlayer;
            switchTurn();
            return true;
        } else if (countTurn == 9) {
            countTurn++;
            O.addDraw();
            X.addDraw();
            return false;
        } else {
            return false;
        }
    }

    public void switchTurn() {
        if (currentPlayer == X) {
            currentPlayer = O;
        } else {
            currentPlayer = X;
        }
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public char[][] getData() {
        return data;
    }

    public boolean checkRow(int row) {
        if (data[row -1 ][0] == data[row - 1][1] && data[row - 1][0] != '-') {
            if (data[row -1 ][1] == data[row - 1][2]) {
                return true;
            }
        }
        return false;
    }

    public boolean checkCol(int col) {
        if (data[0][col - 1] == data[1][col - 1]&& data[0][col - 1] != '-') {
            if (data[1][col - 1] == data[2][col - 1]) {
                return true;
            }
        }
        return false;
    }

    public boolean checkDiagonal1() {
        if (data[0][0] == data[1][1] && data[0][0]!= '-')  {
            if (data[0][0] == data[2][2]) {
                return true;
            }
        }
        return false;
    }

    public boolean checkDiagonal2() {
        if (data[0][2] == data[1][1]&& data[0][2]!= '-') {
            if (data[0][2] == data[2][0]) {
                return true;
            }
        }
        return false;
    }

    public void updateScore() {
        if (win.getName() == O.getName()) {
            O.addWin();
            X.addLose();
        } else if (win.getName() == X.getName()) {
            X.addWin();
            O.addLose();
        }
    }

    public int getCountTurn() {
        return countTurn;
    }

    public Player getPlayer() {
        return player;
    }
}
