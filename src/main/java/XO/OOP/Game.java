package XO.OOP;


import java.util.*;

public class Game {

    Scanner kb = new Scanner(System.in);
    private char Continue = 'Y';
    private Table table;
    private Player X;
    private Player O;
    private int row;
    private int col;

    public Game() {
        O = new Player();
        X = new Player();
        O.setName('O');
        X.setName('X');

    }

    public void startGame() {
        table = new Table(X, O);
    }

    public void showWelcome() {
        System.out.println("Hello welcome to XO Game ");
        System.out.println(" Let's Start");
    }

    public void showTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table.getData()[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " Turn");
    }

    public void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        do {
            System.out.println("Please enter row&colum : ");
            row = kb.nextInt();
            col = kb.nextInt();
        } while (table.checkPos(row, col));
    }
    

    public void inputContinue() {
        System.out.println("Continue??" + "(Y/N): ");
        Continue = kb.next().charAt(0);

    }

    public void showWinner() {
        System.out.println(table.getWinner().getName() + " Win!!!");
    }

    public void showBye() {
        System.out.println("See u later");
    }

    public void showAllScore() {
        System.out.println("X win:" + X.getWin() + " lose" + X.getLose()
                + " Draw: " + X.getDraw());
        System.out.println("O Win:" + O.getWin() + " lose:" +O.getLose()
                + " Draw: " + O.getDraw());
    }

    public void run() {

        while (Continue == 'Y') {
            startGame();
            showWelcome();
            do {
                showTable();
                showTurn();
                inputRowCol();
                table.inputRowCol(row, col);
                table.switchTurn();
            } while (table.checkWin(row, col) == false && table.getCountTurn() < 9);
            showTable();
            if (table.checkWin(row, col)) {
                showWinner();
                table.updateScore();
            }
            showAllScore();
            inputContinue();
            if (Continue == 'N') {
                showBye();
            }
        }
    }
    public char getContinue(){
        return Continue;
    }

}
